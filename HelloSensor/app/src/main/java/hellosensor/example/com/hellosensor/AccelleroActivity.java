package hellosensor.example.com.hellosensor;

import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class AccelleroActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    Sensor accelerometer;
    Boolean initialized;
    private float mLastX, mLastY, mLastZ;
    private final float NOISE = (float) 2.0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accellero);

        initialized = false;


            sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);


    }
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stop() {
        sensorManager.unregisterListener(this, accelerometer);

    }
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView tvX= (TextView)findViewById(R.id.x_axis);
        TextView tvY= (TextView)findViewById(R.id.y_axis);
        TextView tvZ= (TextView)findViewById(R.id.z_axis);
        TextView flatview= (TextView)findViewById(R.id.textAccello);
        ImageView iv = (ImageView)findViewById(R.id.image);
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];

        tvX.setText(String.format("%.2f", x));
        tvY.setText(String.format("%.2f", y));
        tvZ.setText(String.format("%.2f", z));
        initialized = true;
        iv.setVisibility(View.VISIBLE);
        iv.setImageResource(R.drawable.arrowleft);
        if (x > 0.5) {
            flatview.setVisibility(View.INVISIBLE);
            iv.setImageResource(R.drawable.arrowleft);
        } else if (x < -0.5) {
            flatview.setVisibility(View.INVISIBLE);
            iv.setImageResource(R.drawable.arrowright);
        } else {
            flatview.setVisibility(View.VISIBLE);
            iv.setVisibility(View.INVISIBLE);

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
    public void noSensorsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage("Your device doesn't support the Accelleromer.")
                .setCancelable(false)
                .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                });
        alertDialog.show();
    }
}

# HelloSensorGit


Bas-funktionen av kompassen har utvecklats med hjälp av den tutorial som är länkad på moodle. Ytterligare har jag lagt till vibrationssignal när sensorn pekar mot norr (0 grader).
Sen har även en interaktions-feature lagt in där användaren själv kan välja bild genom att klicka på kompassen. Denna har utvecklats genom att lägga till onclick-event på ImageView. 

Inför utvecklingen av accelerometern har jag främst tittat på en accelerometer-tutorial samt en video som behandlar avläsningen av sensorer. Har använt koddelar från båda i utvecklingen som jag utvärderat utifrån design-perspektiv. Se nedan för länkar. Utöver grunduppgiften har jag lagt in bilder samt text beroende på sensorns värden. Vridning åt höger ger pil åt höger, etc. När accelerometern är i nästan plant läge ges uppmaning i form av text att vrida på telefonen för att indikera funktionen för användaren. 

Övrigt har jag provat mig fram och utvärderat olika sätt att placera bilder och knappar för centrering. Icon på appen är även bytt och default stylingen i manifest är ändrad. 


https://www.techrepublic.com/blog/software-engineer/a-quick-tutorial-on-coding-androids-accelerometer/

https://www.youtube.com/watch?v=Rda_5s4rObQ


